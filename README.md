# InletWearTest #

InletWearTest runs a PLC program to cycle the inlet flaps.
It performs the following;  

* Runs a low speed on the open fan to open the inlet
* Waits for airflow to stop
* Runs a medium speed on the close fan to shut the inlet
* Waits for airflow to stop
* Runs a high speed on the close fan to shut the inlet
* Waits for airflow to stop
* Repeat

The cycle count (combining open and close) is maintained.
If the inlet does not open, the program will halt, raise a fault, and must be reset manually.

### How do I get set up? ###

* HMI has primary controls
* All timing intervals are configurable by setting the appropriate reset

### Who do I talk to? ###

* Jack Arney (Developer)
* Nathan Budarick (Test Administrator)
* James Beare (Inlet Designer)
